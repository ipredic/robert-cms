import { createStore, applyMiddleware } from "redux";
import { handleActions } from "redux-actions";
import thunk from "redux-thunk";
import throttle from "lodash/throttle";

import loginSlice from "../containers/Login/loginSlice";
import logoutSlice from "../containers/Logout/logoutSlice";
import { loadState, saveState } from "../helpers/localStorage";
import carsSlice from "../containers/Cars/carsSlice";

const persistedState = loadState();

const initialState = {
  user: {
    accessToken: "",
    isAuthenticated: false
  },
  error: false,
  request: false,
  cars: []
};

const rootReducer = handleActions(
  {
    ...loginSlice,
    ...carsSlice,
    ...logoutSlice
  },
  initialState
);

const store = createStore(rootReducer, persistedState, applyMiddleware(thunk));

store.subscribe(
  throttle(() =>
    saveState({
      ...initialState,
      user: { ...store.getState().user }
    })
  ),
  1000
);

export default store;
