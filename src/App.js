import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Switch } from "react-router-dom";

import LoginFormContainer from "./containers/Login/LoginFormContainer";
import Dashboard from "./containers/Dashboard/Dashboard";
import PrivateRoute from "./components/PrivateRoute";
import Error from "./components/Error";
import Loader from "./components/Loader";

const mapStateToProps = ({ user, error, request }) => ({
  user,
  error,
  request
});

export default connect(mapStateToProps)(
  class App extends Component {
    render() {
      if (this.props.error) {
        return (
          <Error
            content="Doslo je do greske"
            explain="Rifresujte da bi ste pokusali ponovo"
          />
        );
      }

      return (
        <div>
          <Loader show={this.props.request} />
          <Switch>
            <Route exact path="/" component={LoginFormContainer} />
            <PrivateRoute
              path="/carlist"
              component={Dashboard}
              isAuthenticated={this.props.user.isAuthenticated}
            />
          </Switch>
        </div>
      );
    }
  }
);
