import React, { Component } from "react";
import { connect } from "react-redux";
import { Route, Switch, HashRouter } from "react-router-dom";

import NavBar from "../../components/NavBar";
import CarListContainer from "./../Cars/CarListContainer";
import AddCarContainer from "./../AddCar/AddCarContainer";
import UpdateCarContainer from "./../UpdateCarContainer/UpdateCarContainer";

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(
  class Dashboard extends Component {
    render() {
      return (
        <HashRouter>
          <NavBar />
          <div style={{ padding: "20px" }}>
            <Switch>
              <Route path="/carlist" component={CarListContainer} />
              <Route path="/car" component={AddCarContainer} />
              <Route
                path="/update"
                render={props => <UpdateCarContainer {...props} />}
              />
            </Switch>
          </div>
        </HashRouter>
      );
    }
  }
);
