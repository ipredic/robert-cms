export default {
  LOGIN: {
    REQUEST: (state, { payload }) => ({
      ...state,
      request: true
    }),
    SUCCESS: (state, { payload }) => ({
      ...state,
      user: {
        isAuthenticated: true,
        accessToken: payload.access_token
      },
      request: false
    }),
    FAILURE: (state, { payload }) => ({
      ...state,
      request: false,
      error: true
    })
  }
};
