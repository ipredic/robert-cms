import { createActions } from "redux-actions";
import { api } from "../../helpers/api";
import axios from "axios";

export const loginActions = createActions({
  LOGIN: {
    REQUEST: request => ({ request }),
    SUCCESS: access_token => ({ access_token }),
    FAILURE: error => ({ error })
  }
});

//thunk
export const userLogin = credentials => async (dispatch, getState) => {
  dispatch(loginActions.login.request())
  try {
    const response = await axios.post(
      `${api}login`,
      { ...credentials },
      {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*"
        }
      }
    );
    dispatch(loginActions.login.success(response.data.access_token));
  } catch (error) {
    dispatch(loginActions.login.failure());
    window.localStorage.clear();
  }
};
