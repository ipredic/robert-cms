import React, { Component } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";

import LoginForm from "../../components/LoginForm";
import Error from "../../components/Error";
import Loader from "../../components/Loader";
import { userLogin } from "./actions";

const mapDispatchToProps = {
  onUserLogin: userLogin
};

const mapStateToProps = ({ user, error, request }) => ({
  user,
  error,
  request
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class LoginFormContainer extends Component {
    state = {
      username: "",
      password: ""
    };

    handleOnChange = e => {
      this.setState({
        [e.target.name]: e.target.value
      });
    };

    handleSubmitForm = () => {
      this.props.onUserLogin(this.state);
      this.setState({
        username: "",
        password: ""
      });
    };

    render() {
      if (this.props.user.isAuthenticated === true) {
        return <Redirect from="/" to="/carlist" />;
      }

      return (
        <div>
          <Loader show={this.props.request} />
          {this.props.error && (
            <Error
              content="Korisnicko ime ili lozinka nije tacna"
              explain="Pokusajte ponovo"
            />
          )}
          <LoginForm
            username={this.state.username}
            password={this.state.password}
            onChange={e => this.setState({ [e.target.name]: e.target.value })}
            onClick={() => this.handleSubmitForm()}
          />
        </div>
      );
    }
  }
);
