import React, { Component } from "react";
import { connect } from "react-redux";
import { Item, Icon, Input, Divider } from "semantic-ui-react";
import { api } from "../../helpers/api";
import axios from "axios";

import AddCarForm from "../../components/AddCarForm";
import Error from "../../components/Error";
import Loader from "../../components/Loader";
import "../../css/global.css";

const mapStateToProps = ({ user }) => ({ user });

export default connect(mapStateToProps)(
  class UpdateCarContainer extends Component {
    state = {
      carDetails: {
        car_id: 0,
        car_name: "",
        car_km: 0,
        car_price: 0,
        car_ez: "",
        car_ps: "",
        car_treibstoff: "",
        car_farbe: "",
        car_kategorie: "",
        car_aufbau: "",
        car_getriebe: "",
        car_antrieb: "",
        car_turen: 0,
        car_sitze: 0,
        car_beschreibung: ""
      },
      car_images: [],
      uploadedImages: [],
      error: false,
      request: false
    };

    componentDidMount = () => {
      this.handleGetCarById();
    };

    handleGetCarById = async () => {
      this.setState({ request: true });
      try {
        const response = await axios.get(
          `${api}car/${this.props.location.state.car_id}`,
          {
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Origin": "*"
            }
          }
        );
        this.setState({
          carDetails: {
            car_id: response.data.car_id,
            car_name: response.data.car_name,
            car_km: response.data.car_km,
            car_price: response.data.car_price,
            car_ez: response.data.car_ez,
            car_ps: response.data.car_ps,
            car_treibstoff: response.data.car_treibstoff,
            car_farbe: response.data.car_farbe,
            car_kategorie: response.data.car_kategorie,
            car_aufbau: response.data.car_aufbau,
            car_getriebe: response.data.car_getriebe,
            car_antrieb: response.data.car_antrieb,
            car_turen: response.data.car_turen,
            car_sitze: response.data.car_sitze,
            car_beschreibung: response.data.car_beschreibung
          },
          car_images: response.data.car_image,
          request: false
        });
      } catch (error) {
        this.setState({ error: true, request: false });
        window.localStorage.clear();
      }
    };

    handleUpdateCarImage = async img => {
      this.setState({ request: true });
      const accessToken = this.props.user.accessToken;
      try {
        const image = new FormData();
        image.append("image", img);

        const sendImage = await axios.post(
          `${api}image/${this.props.location.state.car_id}`,
          image,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              "Access-Control-Allow-Origin": "*",
              Authorization: `Bearer ${accessToken}`
            }
          }
        );
        this.setState({
          uploadedImages: [...this.state.uploadedImages, img],
          request: false
        });
      } catch (error) {
        this.setState({ error: true, request: false });
        window.localStorage.clear();
      }
    };

    handleGetImgNameFromUrlAndDelete = async imgKey => {
      this.setState({ request: true });
      const accessToken = this.props.user.accessToken;
      const img_name = imgKey.match(/.*\/(.*)$/)[1];

      try {
        const response = await axios.delete(
          `${api}image/${this.props.location.state.car_id}/${img_name}`,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              "Access-Control-Allow-Origin": "*",
              Authorization: `Bearer ${accessToken}`
            }
          }
        );
        this.setState({
          car_images: this.state.car_images.filter(e => e !== imgKey),
          request: false
        });
      } catch (error) {
        this.setState({ error: true, request: false });
        window.localStorage.clear();
      }
    };

    handleDeleteImage = async imgName => {
      this.setState({ request: true });
      const accessToken = this.props.user.accessToken;
      try {
        const response = await axios.delete(
          `${api}image/${this.props.location.state.car_id}/${this.props.location.state.car_id}_${imgName}`,
          {
            headers: {
              "Content-Type": "multipart/form-data",
              "Access-Control-Allow-Origin": "*",
              Authorization: `Bearer ${accessToken}`
            }
          }
        );
        this.setState(prevState => ({
          uploadedImages: prevState.uploadedImages.filter(
            e => e.name !== imgName
          ),
          request: false
        }));
      } catch (error) {
        this.setState({ error: true, request: false });
        window.localStorage.clear();
      }
    };

    handleUpdateCar = async car_id => {
      this.setState({ request: true });
      const accessToken = this.props.user.accessToken;
      try {
        const response = await axios.put(
          `${api}car/${car_id}`,
          { ...this.state.carDetails },
          {
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Origin": "*",
              Authorization: `Bearer ${accessToken}`
            }
          }
        );
        this.setState({ request: false });
        this.props.history.push("carlist");
      } catch (error) {
        this.setState({ error: true, request: false });
        window.localStorage.clear();
      }
    };

    render() {
      if (this.state.error) {
        return (
          <Error
            content="Doslo je do greske"
            explain="Rifresujte da bi ste pokusali ponovo"
          />
        );
      }
      if (this.state.carDetails) {
        return (
          <>
            <Loader show={this.state.request} />
            <Input
              label="Izaberi slike auta"
              icon="upload"
              className="inputfile"
              type="file"
              onChange={e => this.handleUpdateCarImage(e.target.files[0])}
            />
            <Divider />
            <h1>Stare slike</h1>
            {this.state.car_images
              ? this.state.car_images.map((img, idx) => (
                  <div key={idx}>
                    <Item.Image size="small" src={img} />
                    <Icon
                      name="trash"
                      onClick={() => this.handleGetImgNameFromUrlAndDelete(img)}
                    />
                  </div>
                ))
              : null}
            <h1>Nove slike</h1>
            {this.state.uploadedImages &&
              this.state.uploadedImages.map(img => (
                <div key={img.name}>
                  <Item.Image
                    key={img.name}
                    size="small"
                    src={URL.createObjectURL(img)}
                  />
                  <Icon
                    name="trash"
                    onClick={() => this.handleDeleteImage(img.name)}
                  />
                </div>
              ))}
            <AddCarForm
              car_id={this.state.carDetails.car_id}
              car_name={this.state.carDetails.car_name}
              car_km={this.state.carDetails.car_km}
              car_price={this.state.carDetails.car_price}
              car_ez={this.state.carDetails.car_ez}
              car_ps={this.state.carDetails.car_ps}
              car_treibstoff={this.state.carDetails.car_treibstoff}
              car_farbe={this.state.carDetails.car_farbe}
              car_kategorie={this.state.carDetails.car_kategorie}
              car_aufbau={this.state.carDetails.car_aufbau}
              car_getriebe={this.state.carDetails.car_getriebe}
              car_antrieb={this.state.carDetails.car_antrieb}
              car_turen={this.state.carDetails.car_turen}
              car_sitze={this.state.carDetails.car_sitze}
              car_beschreibung={this.state.carDetails.car_beschreibung}
              onChange={e => {
                const { carDetails } = { ...this.state };
                const currentState = carDetails;
                const { name, value } = e.target;
                currentState[name] = value;
                this.setState({ carDetails: currentState });
              }}
              onClick={() => this.handleUpdateCar(this.state.carDetails.car_id)}
              content="Azuriraj"
            />
          </>
        );
      }
      return <div>Loading...</div>;
    }
  }
);
