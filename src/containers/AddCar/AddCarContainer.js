import React, { Component } from "react";
import { connect } from "react-redux";
import AddCarForm from "../../components/AddCarForm";
import { Item, Icon, Input, Divider } from "semantic-ui-react";
import Error from "../../components/Error";
import Loader from "../../components/Loader";
import axios from "axios";
import { api } from "./../../helpers/api";
import "./../../css/global.css";

const mapStateToProps = ({ user, accessToken }) => ({
  user,
  accessToken
});

const idNumber = () => {
  let unique = new Date().getTime();
  let uniqueNum = unique & 0xffffffff;
  return Math.abs(uniqueNum);
};

export default connect(mapStateToProps)(
  class AddCarContainer extends Component {
    state = {
      carDetails: {
        car_id: idNumber(),
        car_name: "",
        car_km: 0,
        car_price: 0,
        car_ez: "",
        car_ps: "",
        car_treibstoff: "",
        car_farbe: "",
        car_kategorie: "",
        car_aufbau: "",
        car_getriebe: "",
        car_antrieb: "",
        car_turen: 0,
        car_sitze: 0,
        car_beschreibung: ""
      },
      images: [],
      error: false,
      request: false
    };

    handleDeleteImage = imgName => {
      this.setState(prevState => ({
        images: prevState.images.filter(i => i.name !== imgName)
      }));
    };

    handleSubmitForm = async car_id => {
      this.setState({ request: true });
      try {
        const { carDetails } = { ...this.state };
        const accessToken = this.props.user.accessToken;

        const sendCarDetails = await axios.post(
          `${api}car/${car_id}`,
          { accessToken, ...carDetails },
          {
            headers: {
              "Content-Type": "application/json",
              "Access-Control-Allow-Origin": "*",
              Authorization: `Bearer ${accessToken}`
            }
          }
        );

        const image = new FormData();
        for (const file of this.state.images) {
          image.append("image", file);
        }

        const sendImage = await axios.post(`${api}image/${car_id}`, image, {
          headers: {
            "Content-Type": "multipart/form-data",
            "Access-Control-Allow-Origin": "*",
            Authorization: `Bearer ${accessToken}`
          }
        });
        this.setState({ request: false });
        return this.props.history.push(`${api}carlist`);
      } catch (error) {
        this.setState({ error: true, request: false });
        window.localStorage.clear();
      }
    };

    render() {
      if (this.state.error) {
        return (
          <Error
            content="Doslo je do greske"
            explain="Rifresujte da bi ste pokusali ponovo"
          />
        );
      }
      return (
        <div>
          <Loader show={this.state.request} />
          <div className="upload__container">
            <Input
              label="Izaberi slike auta"
              icon="upload"
              className="inputfile"
              type="file"
              onChange={e =>
                this.setState({
                  images: [...this.state.images, e.target.files[0]]
                })
              }
            />
            <Divider />
            <div className="upload__cars__container">
              {this.state.images.length > 0 &&
                this.state.images.map(i => (
                  <div key={i.name}>
                    <Item.Image
                      className="image"
                      size="small"
                      src={URL.createObjectURL(i)}
                    />
                    <Icon
                      name="trash"
                      onClick={() => this.handleDeleteImage(i.name)}
                    />
                  </div>
                ))}
            </div>
          </div>
          <AddCarForm
            car_id={this.state.carDetails.car_id}
            car_name={this.state.carDetails.car_name}
            car_km={this.state.carDetails.car_km}
            car_price={this.state.carDetails.car_price}
            car_ez={this.state.carDetails.car_ez}
            car_ps={this.state.carDetails.car_ps}
            car_treibstoff={this.state.carDetails.car_treibstoff}
            car_farbe={this.state.carDetails.car_farbe}
            car_kategorie={this.state.carDetails.car_kategorie}
            car_aufbau={this.state.carDetails.car_aufbau}
            car_getriebe={this.state.carDetails.car_getriebe}
            car_antrieb={this.state.carDetails.car_antrieb}
            car_turen={this.state.carDetails.car_turen}
            car_sitze={this.state.carDetails.car_sitze}
            car_beschreibung={this.state.carDetails.car_beschreibung}
            onChange={e => {
              const { carDetails } = { ...this.state };
              const currentState = carDetails;
              const { name, value } = e.target;
              currentState[name] = value;
              this.setState({ carDetails: currentState });
            }}
            onClick={() => this.handleSubmitForm(this.state.carDetails.car_id)}
            content="Dodaj auto +"
          />
        </div>
      );
    }
  }
);
