export default {
  CARS: {
    REQUEST: (state, { payload }) => ({
      ...state,
      request: true
    }),
    SUCCESS: (state, { payload }) => ({
      ...state,
      cars: payload.cars,
      request: false
    }),
    FAILURE: (state, { payload }) => ({
      ...state,
      error: true,
      request: false
    })
  },
  DELETE: {
    REQUEST: (state, { payload }) => ({
      ...state,
      request: true
    }),
    SUCCESS: (state, { payload }) => ({
      ...state,
      request: false,
      cars: state.cars.filter(car => car.car_id !== payload.car_id)
    }),
    FAILURE: (state, { payload }) => ({
      ...state,
      error: true,
      request: false
    })
  }
};
