import React, { PureComponent } from "react";
import { connect } from "react-redux";

import { getCars, deleteCar } from "./actions";
import CarWrapper from "../../components/CarWrapper";
import Loading from "../../components/Loader";

const mapStateToProps = ({ cars, error, request }) => ({
  cars,
  error,
  request
});

const mapDispatchToProps = {
  onGetCars: getCars,
  onCarDelete: deleteCar
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(
  class CarContainer extends PureComponent {
    componentDidMount = () => {
      this.props.onGetCars();
    };

    render() {
      if (this.props.cars) {
        return this.props.cars.map(car => (
          <CarWrapper
            key={car.car_id}
            carId={car.car_id}
            carImage={car.car_image[0]}
            carName={car.car_name}
            carKategorie={car.car_kategorie}
            carTreibstoff={car.car_treibstoff}
            carAufbau={car.car_aufbau}
            carEz={car.car_ez}
            carPs={car.car_ps}
            carKm={car.car_km}
            carPrice={car.car_price}
            onCarDelete={() => this.props.onCarDelete(car.car_id)}
            onCarUpdate={() =>
              this.props.history.push({
                pathname: `/update`,
                state: { car_id: car.car_id }
              })
            }
          />
        ));
      }
      return <Loading show={this.props.show} />;
    }
  }
);
