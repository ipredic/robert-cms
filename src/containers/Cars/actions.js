import { createActions } from "redux-actions";
import { api } from "../../helpers/api";
import axios from "axios";

export const carActions = createActions({
  CARS: {
    REQUEST: request => ({ request }),
    SUCCESS: cars => ({ cars }),
    FAILURE: error => ({ error })
  },
  DELETE: {
    REQUEST: request => ({ request }),
    SUCCESS: car_id => ({ car_id }),
    FAILURE: x => x
  }
});

export const getCars = () => async (dispatch, getState) => {
  dispatch(carActions.cars.request());
  try {
    const response = await axios.get(`${api}carlist`);
    const cars = response.data.cars;
    dispatch(carActions.cars.success(cars));
  } catch (error) {
    dispatch(carActions.cars.failure());
    window.localStorage.clear();
  }
};

export const deleteCar = car_id => async (dispatch, getState) => {
  dispatch(carActions.delete.request());
  try {
    const accessToken = getState().user.accessToken;
    const response = await axios.delete(`${api}car/${car_id}`, {
      headers: {
        "Content-Type": "application/json",
        "Access-Control-Allow-Origin": "*",
        Authorization: `Bearer ${accessToken}`
      }
    });
    dispatch(carActions.delete.success(car_id));
  } catch (error) {
    dispatch(carActions.delete.failure());
    window.localStorage.clear();
  }
};
