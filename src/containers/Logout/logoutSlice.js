export default {
  LOGOUT: {
    REQUEST: (state, { payload }) => ({
      ...state,
      request: true
    }),
    SUCCESS: state => ({
      ...state,
      request: false,
      user: {
        isAuthenticated: false,
        accessToken: ""
      }
    }),
    FAILURE: (state, { payload }) => ({
      ...state,
      request: false,
      error: true
    })
  }
};
