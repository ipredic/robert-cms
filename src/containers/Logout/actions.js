import { createActions } from "redux-actions";
import { api } from "../../helpers/api";
import axios from "axios";

export const logoutActions = createActions({
  LOGOUT: {
    REQUEST: request => ({ request }),
    SUCCESS: x => x,
    FAILURE: error => ({ error })
  }
});

export const userLogOut = () => async (dispatch, getState) => {
  dispatch(logoutActions.logout.request());
  try {
    const accessToken = getState().user.accessToken;
    const response = await axios.post(
      `${api}logout`,
      { accessToken },
      {
        headers: {
          "Content-Type": "application/json",
          "Access-Control-Allow-Origin": "*",
          Authorization: `Bearer ${accessToken}`
        }
      }
    );
    dispatch(logoutActions.logout.success());
    window.localStorage.clear();
  } catch (error) {
    ///ignore for now
    window.localStorage.clear();
    console.log("ERROR:", error);
  }
};
