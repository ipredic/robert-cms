import React from "react";
import { Item, Button, Divider, Icon, Label } from "semantic-ui-react";

const CarWrapper = ({
  carImage,
  carName,
  carKategorie,
  carTreibstoff,
  carAufbau,
  carEz,
  carPs,
  carKm,
  carPrice,
  onCarDelete,
  onCarUpdate
}) => (
  <Item.Group divided>
    <Item>
      <Item.Image size="small" src={carImage} />
      <Item.Content>
        <Item.Header>{carName}</Item.Header>
        <Item.Meta>
          <span className="price">Auto Podaci</span>
        </Item.Meta>
        <Item.Description>
          {carKategorie}, {carTreibstoff}, {carAufbau}
        </Item.Description>
        <Item.Extra>
          <Button onClick={onCarDelete} color="red" floated="right">
            Obrisi auto
            <Icon name="right chevron" />
          </Button>
          <Button onClick={onCarUpdate} secondary floated="right">
            Izmeni/Vidi Detalje
            <Icon name="right chevron" />
          </Button>
          <Label.Group tag>
            <Label color="black">EZ: {carEz}</Label>
            <Label color="black">PS: {carPs}</Label>
            <Label color="black">KM: {carKm}</Label>
            <Label color="red">{carPrice} €</Label>
          </Label.Group>
        </Item.Extra>
      </Item.Content>
    </Item>
    <Divider />
  </Item.Group>
);

export default CarWrapper;
