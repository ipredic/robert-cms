import React from "react";
import { Dimmer, Loader } from "semantic-ui-react";

const Loading = ({ show }) => {
  if (show) {
    return (
      <Dimmer active inverted>
        <Loader size="large">Loading</Loader>
      </Dimmer>
    );
  }
  return null;
};

export default Loading;
