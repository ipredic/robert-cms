import React from "react";
import { Input, Form, TextArea, Button } from "semantic-ui-react";
import "./addCarForm.css";

const AddCar = ({
  onChange,
  onClick,
  car_id,
  car_name,
  car_km,
  car_price,
  car_ez,
  car_ps,
  car_treibstoff,
  car_farbe,
  car_kategorie,
  car_aufbau,
  car_getriebe,
  car_antrieb,
  car_turen,
  car_sitze,
  car_beschreibung,
  content
}) => (
  <Form className="car__form">
    <Input type="hidden" name="car_id" value={car_id} />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Naziv auta" }}
      placeholder="Unesi naziv auta..."
      name="car_name"
      value={car_name}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "KM" }}
      placeholder="Unesi kilometrazu auta..."
      type="number"
      name="car_km"
      value={car_km}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "€" }}
      placeholder="Unesi cenu auta..."
      type="number"
      name="car_price"
      value={car_price}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "EZ" }}
      placeholder="05/2018"
      name="car_ez"
      value={car_ez}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "PS" }}
      placeholder="224(123KW)"
      name="car_ps"
      value={car_ps}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Gorivo" }}
      placeholder="Unesi vrstu goriva..."
      name="car_treibstoff"
      value={car_treibstoff}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Boja auta" }}
      placeholder="Unesi boju auta..."
      name="car_farbe"
      value={car_farbe}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Kategorija" }}
      placeholder="Nov ili Polovan auto..."
      name="car_kategorie"
      value={car_kategorie}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Tip auta" }}
      placeholder="Limosina, Kabrio, Karavan..."
      name="car_aufbau"
      value={car_aufbau}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Menjac" }}
      placeholder="Automatic, Saltac..."
      name="car_getriebe"
      value={car_getriebe}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Pogon" }}
      placeholder="Allrad, Front, Hinten..."
      name="car_antrieb"
      value={car_antrieb}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Vrata" }}
      placeholder="3 ili 4 ili 5..."
      type="number"
      name="car_turen"
      value={car_turen}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <Input
      iconPosition="left"
      label={{ basic: true, content: "Sedista" }}
      placeholder="3 ili 4 ili 5..."
      type="number"
      name="car_sitze"
      value={car_sitze}
      onChange={onChange}
      required
      className="car__form__input"
    />
    <TextArea
      placeholder="Dodatni podaci o autu na primer dodatna oprema, stanje, ulje i td..."
      style={{ minHeight: 200, width: "50%" }}
      name="car_beschreibung"
      value={car_beschreibung}
      onChange={onChange}
    />
    <Button
      className="car__form__button"
      color="black"
      size="large"
      onClick={onClick}
    >
      {content}
    </Button>
  </Form>
);

export default AddCar;
