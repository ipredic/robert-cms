import React, { Component } from "react";
import { connect } from "react-redux";
import { Button } from "semantic-ui-react";
import { Link } from "react-router-dom";

import { userLogOut } from "../containers/Logout/actions";

const mapDispatcToProps = {
  onUserLogOut: userLogOut
};

export default connect(
  null,
  mapDispatcToProps
)(
  class NavigationBar extends Component {
    handleLogOut = () => {
      this.props.onUserLogOut();
    };

    render() {
      return (
        <div className="ui inverted segment">
          <div className="ui inverted pointing secondary menu">
            <Link to="/carlist">
              <li className="item">
                <Button inverted>Svi Automobili</Button>
              </li>
            </Link>
            <Link to="/car">
              <li className="item">
                <Button inverted>Dodaj Automobil +</Button>
              </li>
            </Link>
            <Link to="/" onClick={() => this.handleLogOut()}>
              <li className="item right">
                <Button inverted>Odjavi se</Button>
              </li>
            </Link>
          </div>
        </div>
      );
    }
  }
);
