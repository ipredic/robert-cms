import React from "react";
import { Message, Button } from "semantic-ui-react";
import "../css/global.css";

const Error = ({ content, explain }) => (
  <Message negative className="error_msg">
    <Message.Header>
      <h1>{content}!</h1>
    </Message.Header>
    <h2>{explain}</h2>
    <Button color="teal" onClick={() => window.location.reload()}>
      Rifresujte
    </Button>
  </Message>
);

export default Error;
