import React from "react";
import { Button, Form, Grid, Segment, Image } from "semantic-ui-react";
import Logo from "../components/logo.png";

const LoginForm = ({ username, password, onChange, onClick }) => (
  <Grid textAlign="center" style={{ height: "100vh" }} verticalAlign="middle">
    <Grid.Column style={{ maxWidth: 450 }}>
      <Image src={Logo} style={{ margin: "auto" }} />
      <Form size="large">
        <Segment stacked>
          <Form.Input
            fluid
            name="username"
            icon="user"
            iconPosition="left"
            placeholder="Korisnicko ime"
            value={username}
            onChange={onChange}
          />
          <Form.Input
            fluid
            name="password"
            icon="lock"
            iconPosition="left"
            placeholder="Lozinka"
            type="password"
            value={password}
            onChange={onChange}
          />
          <Button color="teal" fluid size="large" onClick={onClick}>
            Uloguj se
          </Button>
        </Segment>
      </Form>
    </Grid.Column>
  </Grid>
);

export default LoginForm;
